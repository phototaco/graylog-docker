#!/bin/bash

# Check if jq is installed, if not, install it
if ! command -v jq &> /dev/null; then
  echo "jq could not be found, installing..."
  sudo yum install -y jq
  if [ $? -ne 0 ]; then
    echo "Failed to install jq. Please install it manually."
    exit 1
  fi
fi

# Create Docker network
docker network create graylog_net

# Start MongoDB container
docker run -d \
  --name mongodb \
  --hostname mongo \
  --network graylog_net \
  --memory 1.5g \
  --cpus 0.5 \
  -p 27017:27017 \
  -v mongodb_data:/data/db \
  --restart always \
  mongo:latest

# Start OpenSearch container
docker run -d \
  --name opensearch \
  --hostname opensearch \
  --network graylog_net \
  --memory 3.5g \
  --cpus 1 \
  --ulimit memlock=-1:-1 \
  --ulimit nofile=65536:65536 \
  -p 8999:8999/tcp \
  -p 9200:9200/tcp \
  -p 9300:9300/tcp \
  -e "OPENSEARCH_JAVA_OPTS=-Xms2g -Xmx2g" \
  -e "bootstrap.memory_lock=true" \
  -e "discovery.type=single-node" \
  -e "action.auto_create_index=false" \
  -e "plugins.security.ssl.http.enabled=false" \
  -e "plugins.security.disabled=true" \
  -e "OPENSEARCH_INITIAL_ADMIN_PASSWORD=ThisSuck5R0ck5!" \
  -v os_data:/usr/share/opensearch/data \
  --restart always \
  opensearchproject/opensearch:latest

# Wait for Opensearch to be up
echo "Waiting for opensearch to be available..."
until docker run --rm --network graylog_net curlimages/curl:latest curl -s -f -o /dev/null http://opensearch:9200/; do
  echo "Waiting for opensearch..."
  sleep 5
done
echo "opensearch is up."

# Start Graylog container
docker run -d \
  --name graylog \
  --hostname graylog \
  --network graylog_net \
  --memory 2.5g \
  --cpus 0.5 \
  --ulimit memlock=-1:-1 \
  --ulimit nofile=65536:65536 \
  -p 5044:5044/tcp \
  -p 5140:5140/udp \
  -p 5140:5140/tcp \
  -p 5555:5555/tcp \
  -p 5555:5555/udp \
  -p 9000:9000/tcp \
  -p 12201:12201/tcp \
  -p 12201:12201/udp \
  -p 13301:13301/tcp \
  -p 13302:13302/tcp \
  -e GRAYLOG_NODE_ID_FILE=/usr/share/graylog/data/config/node-id \
  -e GRAYLOG_PASSWORD_SECRET=0Efj79feOE7QS9QCP6N9AwnIjy5gUaYgsVUxcbK407DLHnDpMfoL0N5urS9j7J+o \
  -e GRAYLOG_ROOT_PASSWORD_SHA2=8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918 \
  -e GRAYLOG_HTTP_BIND_ADDRESS=0.0.0.0:9000 \
  -e GRAYLOG_HTTP_EXTERNAL_URI=http://localhost:9000/ \
  -e GRAYLOG_ELASTICSEARCH_HOSTS=http://opensearch:9200 \
  -e GRAYLOG_MONGODB_URI=mongodb://mongodb:27017/graylog \
  -e GRAYLOG_DATANODE_ENABLE_TLS=false \
  -e GRAYLOG_DATANODE_HTTP_ENABLE_TSL=false \
  -e GRAYLOG_REST_ENABLE_TLS=false \
  -e GRAYLOG_WEB_ENABLE_TLS=false \
  -e GRAYLOG_ROTATION_STRATEGY=size \
  -e GRAYLOG_ELASTICSEARCH_MAX_SIZE_PER_INDEX=1073741824 \
  -e GRAYLOG_RETENTION_STRATEGY=delete \
  -e GRAYLOG_ELASTICSEARCH_MAX_NUMEBER_OF_INDICES=5 \
  -e GRAYLOG_MESSAGE_JOURNAL_MAX_SIZE=1500MB \
  -e GRAYLOG_TELEMETRY_ENABLED=false \
  -v graylog_data:/usr/share/graylog/data/data \
  -v graylog_journal:/usr/share/graylog/data/journal \
  --restart always \
  graylog/graylog:6.0.2

# Wait for Graylog to be available
echo "Waiting for Graylog to be available..."
until curl -u admin:admin -H 'X-Requested-By: curl' -s -f -o /dev/null http://localhost:9000/api/system/cluster/nodes; do
  echo "Waiting for Graylog..."
  sleep 5
done
echo "Graylog is up."

# Get all index set IDs
INDEX_SET_IDS=$(curl -u admin:admin -H 'X-Requested-By: curl' -s http://localhost:9000/api/system/indices/index_sets | jq -r '.index_sets[].id')

# Update each index set with the desired settings
for ID in $INDEX_SET_IDS; do
  echo "Updating index set $ID..."
  curl -u admin:admin -X PUT -H 'Content-Type: application/json' \
    -H 'X-Requested-By: curl' \
    -d '{
      "title": "Default index set",
      "description": "The default index set",
      "index_prefix": "graylog",
      "shards": 1,
      "replicas": 0,
      "rotation_strategy_class": "org.graylog2.indexer.rotation.strategies.SizeBasedRotationStrategy",
      "rotation_strategy": {
        "type": "org.graylog2.indexer.rotation.strategies.SizeBasedRotationStrategyConfig",
        "max_size": 1073741824
      },
      "retention_strategy_class": "org.graylog2.indexer.retention.strategies.DeletionRetentionStrategy",
      "retention_strategy": {
        "type": "org.graylog2.indexer.retention.strategies.DeletionRetentionStrategyConfig",
        "max_number_of_indices": 2
      },
      "index_optimization_max_num_segments": 1,
      "index_optimization_disabled": false,
      "field_type_refresh_interval": 5000,
      "writable": true
    }' \
    http://localhost:9000/api/system/indices/index_sets/$ID
done

echo "Index settings configured for all index sets."

# Looks like the Syslog TCP and UDP ports are setup by default now?

# Configure Syslog TCP input
curl -u admin:admin -X POST -H 'Content-Type: application/json' \
  -H 'X-Requested-By: curl' \
  -d '{
    "title": "Syslog TCP",
    "global": true,
    "type": "org.graylog2.inputs.syslog.tcp.SyslogTCPInput",
    "configuration": {
      "port": 5140,
      "bind_address": "0.0.0.0",
      "recv_buffer_size": 1048576,
      "tcp_keepalive": false,
      "use_null_delimiter": false,
      "number_worker_threads": 4,
      "override_source": null,
      "force_rdns": false,
      "allow_override_date": true,
      "expand_structured_data": false,
      "store_full_message": true
    }
  }' \
  http://localhost:9000/api/system/inputs

# Configure Syslog UDP input
curl -u admin:admin -X POST -H 'Content-Type: application/json' \
  -H 'X-Requested-By: curl' \
  -d '{
    "title": "Syslog UDP",
    "global": true,
    "type": "org.graylog2.inputs.syslog.udp.SyslogUDPInput",
    "configuration": {
      "port": 5140,
      "bind_address": "0.0.0.0",
      "recv_buffer_size": 262144,
      "override_source": null,
      "force_rdns": false,
      "allow_override_date": true,
      "expand_structured_data": false,
      "store_full_message": true
    }
  }' \
  http://localhost:9000/api/system/inputs

  # Configure GELF UDP input
curl -u admin:admin -X POST -H 'Content-Type: application/json' \
  -H 'X-Requested-By: curl' \
  -d '{
    "title": "GELF UDP",
    "global": true,
    "type": "org.graylog2.inputs.gelf.udp.GELFUDPInput",
    "configuration": {
      "port": 12201,
      "bind_address": "0.0.0.0",
      "recv_buffer_size": 262144,
      "override_source": null,
      "decompress_size_limit": 8388608,
      "number_worker_threads": 2,
      "tls_cert_file": "",
      "tls_key_file": "",
      "tls_key_password": "",
      "tls_enable": false,
      "tls_client_auth": "disabled"
    }
  }' \
  http://localhost:9000/api/system/inputs

echo "TCP and UDP inputs configured."
