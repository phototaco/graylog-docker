#Launch and Configure Minimal Graylog 
## Start Containers
You can start the containers using `docker run` commands:
- *nix: `launch.sh`

or 

`docker-compose up`

1. Configure a few Inputs

    More inputs may need to be configured so that the products can send logs to Graylog. Here is how to configure a few common ones:

    Syslog TCP:
    1. System -> Inputs
    2. Select input: Syslog TCP -> Launch new input
    3. Title: Syslog TCP
    4. Scroll to bottom
    5. Launch Input

    Syslog UDP:
    1. System -> Inputs
    2. Select input: Syslog UDP -> Launch new input
    3. Title: Syslog UDP
    4. Scroll to bottom
    5. Launch Input

    GELF TCP (log4j/slf4j logs)
    1. System -> Inputs
    2. Select input: GELF TCP -> Launch new input
    3. Title GELF TCP
    4. Scroll to bottom
    5. Launch Input

## Hardware Requirements
The minimal requirements for running the stack needed to make graylog functional is:
    
1. 4GB of RAM
    - 0.5 for MongoDB
    - 1GB for Graylog Server
    - 1.5GB for Graylog Data Node
2. 2 CPUs
3. 8GB disk

    NOTE: If more disk can be made available then the indexes can be made to rotate less frequently (done in the curl commands at the end of launch.sh) and the MESSAGE_JOURNAL_MAX_SIZE can be increased.