#!/bin/bash

colima start \
--profile graylog \
--activate \
--arch aarch64 \
--cpu 2 \
--disk 8 \
--memory 4 \
--mount ${HOME}:w \
--mount-inotify \
--ssh-agent \
--vm-type vz \
--vz-rosetta \
--dns=192.168.1.1 \
--network-address \
--verbose